import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireAuth } from 'angularfire2/auth';

import { HomePage } from '../pages/home/home';
import { ServicePage } from '../pages/service/service';
import { ListPage } from '../pages/list/list';
import { ChatPage } from '../pages/chat/chat';
import { MapaPage } from '../pages/mapa/mapa';
import { MiembrosPage } from '../pages/miembros/miembros';
import { PerfilPage } from '../pages/perfil/perfil';
import { LoginPage } from '../pages/login/login';
import { TelefonosPage } from '../pages/telefonos/telefonos';
import { AyudaPage } from '../pages/ayuda/ayuda';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{icon:string, title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private fire:AngularFireAuth) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { icon:'car',title:'Servicio', component: MapaPage},
      {icon:'car', title: 'Servicio 2', component: ServicePage},
      { icon:'clock',title:'Historial', component: MiembrosPage},
      { icon:'people',title:'Afiliaciones', component: TelefonosPage},
      { icon:'help',title:'Ayuda', component: AyudaPage}
    ];

  }
  goPerfil(){
    this.nav.setRoot(PerfilPage);
  }
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  logOut(){
    this.nav.setRoot(LoginPage);
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
