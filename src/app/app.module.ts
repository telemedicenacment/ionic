import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { NavController } from 'ionic-angular';
import { HttpModule } from '@angular/http';
 
import { Http } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ServicePage } from '../pages/service/service';
import { ListPage } from '../pages/list/list';
import { ChatPage } from '../pages/chat/chat';
import { MapaPage } from '../pages/mapa/mapa';
import { MiembrosPage } from '../pages/miembros/miembros';
import { PerfilPage } from '../pages/perfil/perfil';
import { PostsPage } from '../pages/posts/posts';
import { LoginPage } from '../pages/login/login';
import { TelefonosPage } from '../pages/telefonos/telefonos';
import { AyudaPage } from '../pages/ayuda/ayuda';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FirebaseProvider } from './../providers/firebase/firebase';

import { AuthService } from '../providers/auth-service';

export const firebaseConfig = {
    apiKey: "AIzaSyCAqFbfi2tvi28ZWeUuMAFtBGysSe9ZICk",
    authDomain: "orion-6d6cd.firebaseapp.com",
    databaseURL: "https://orion-6d6cd.firebaseio.com",
    projectId: "orion-6d6cd",
    storageBucket: "orion-6d6cd.appspot.com",
    messagingSenderId: "49065992138"
};

import {
 GoogleMaps
} from '@ionic-native/google-maps';
import { WpProvider } from '../providers/wp/wp';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ChatPage,
    MapaPage,
    MiembrosPage,
    PerfilPage,
    PostsPage,
    LoginPage,
    TelefonosPage,
    AyudaPage,
    ServicePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ChatPage,
    MapaPage,
    MiembrosPage,
    PerfilPage,
    PostsPage,
    LoginPage,
    TelefonosPage,
    AyudaPage,
    ServicePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GoogleMaps,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    WpProvider,
    FirebaseProvider,
    FirebaseProvider
  ]
})
export class AppModule {}
