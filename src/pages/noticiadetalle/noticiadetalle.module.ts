import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoticiadetallePage } from './noticiadetalle';

@NgModule({
  declarations: [
    NoticiadetallePage,
  ],
  imports: [
    IonicPageModule.forChild(NoticiadetallePage),
  ],
  exports: [
  	NoticiadetallePage,
  ]
})
export class NoticiadetallePageModule {}