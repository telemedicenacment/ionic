import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Post } from '../../providers/wp/wp';


/**
 * Generated class for the NoticiadetallePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-noticiadetalle',
  templateUrl: 'noticiadetalle.html',
})
export class NoticiadetallePage {

	post: Post;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.post = navParams.get('post');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NoticiadetallePage');
  }

}
