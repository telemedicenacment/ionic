import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MiembrosPage } from './miembros';

@NgModule({
  declarations: [
    MiembrosPage,
  ],
  imports: [
    IonicPageModule.forChild(MiembrosPage),
  ],
})
export class MiembrosPageModule {}
