import { Component } from '@angular/core'; 
import { AlertController, LoadingController, ModalController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PerfilPage } from '../perfil/perfil';
import { MapaPage } from '../mapa/mapa';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
/**
 * Generated class for the MiembrosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-miembros',
  templateUrl: 'miembros.html',
})
export class MiembrosPage {
	items=[];
  	posts=[];
  	user={};

  	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		public afd: AngularFireDatabase,
		public modalCtrl:ModalController)
   {

		this.posts = [
		];
		this.user ={
		    "deviceToken" : "eDvqANggYwM:APA91bFKCT4RYvuAMI90gGFzbms5-ndQrtiPwe5BUo-jtVwMBcU3e5XcK1nIaWaLo7f6fPnBYDa8I-xqjW5Y7tUE9VgKbnarrVIiZj3ukNtNC_oI5fUhQB5zjyWEGnYSwUX1SKIuwT8G",
		    "email" : "nzubiaur@enacment.com",
		    "userImage" : "https://cache-graphicslib.viator.com/graphicslib/thumbs360x240/2635/SITours/pompeii-and-amalfi-coast-semi-private-day-trip-from-rome-in-rome-350763.jpg",
		    "username" : "Nícolas"
		 }
		 this.items=[
		 	{
		    date : Date.now(),
		    userImage : "img/avatar1.jpg",
		    username : "Sebastian R.",
		    status : "Completo",
		    amount: 375
		 	},
		 	{
		    date : Date.now(),
		    userImage : "img/default-avatar.png",
		    username : "Juan José F.",
		    status : "Completo",
		    amount: 420
		 	},
		 	{
		    date : Date.now(),
		    userImage : "img/avatar2.jpg",
		    username : "Roberto H.",
		    status : "Cancelado",
		    amount: 0
			}

		 ]
  	}


	ionViewDidLoad() {
	}
    mapa(){
    this.navCtrl.setRoot(MapaPage);
  }

		reportar() {
		  let alert2 = this.alertCtrl.create({
		    title: 'Reportar Servicio',
		    inputs: [
		      {
		        name: 'username',
		        placeholder: 'Motivo de reporte',
		        type: 'text'
		      } ],
		    buttons: [
		      {
		        text: 'Cancelar',
		        role: 'cancel',
		        handler: data => {
		          console.log('Cancel clicked');
		        }
		      },
		      {
		        text: 'Enviar',
		        handler: data => {
		        	
					let loading = this.loadingCtrl.create({
						content: 'Enviando reporte...'
					});
					loading.present();
				    let TIME_IN_MS = 3000;
				    setTimeout( () => {
				    	let alert = this.alertCtrl.create({
					      title: 'Reporte Enviado',
					      subTitle: 'Reivsaremos tu caso y nos pondremos en contacto contigo.',
					      buttons: ['Ok']
					      });
					      alert.present();
				    loading.dismiss();
				    }, TIME_IN_MS);
		        }
		      }
		    ]
		  });
		  alert2.present();
		}

}
