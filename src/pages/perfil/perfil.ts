import { Component } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
/**
 * Generated class for the PerfilPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  items=[];
    posts=[];
    user={};
    users=[];
    constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public viewCtrl: ViewController,
    public afd: AngularFireDatabase)
   {
    this.user ={
        "deviceToken" : "eDvqANggYwM:APA91bFKCT4RYvuAMI90gGFzbms5-ndQrtiPwe5BUo-jtVwMBcU3e5XcK1nIaWaLo7f6fPnBYDa8I-xqjW5Y7tUE9VgKbnarrVIiZj3ukNtNC_oI5fUhQB5zjyWEGnYSwUX1SKIuwT8G",
        "email" : "nzubiaur@enacment.com",
        "userImage" : "https://pbs.twimg.com/profile_images/651139349870841856/oQKVlnEI_400x400.jpg",
        "username" : "Nícolas Z."
     }
    const posts$ : FirebaseListObservable<any> = this.afd.list('user-posts/7ykDx8EPcWMebnoO41DlVdDGKnG3', {

      });
    posts$.subscribe(
      val => this.items=val 
      );
  }
  	closeModal(){ 
		console.log("Cerrarmodal");
		this.viewCtrl.dismiss();
 	}
  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

  goBack() {
    this.navCtrl.pop();
  }
  asklocation(){
      //TODO: Pedir ubicación GPS y guardar en Firebase
    let loading = this.loadingCtrl.create({
      content: 'Solicitando Ubicación...'
    });
    loading.present();
    let TIME_IN_MS = 5000;
    let alert = setTimeout( () => {
      loading.dismiss();
      let alert = this.alertCtrl.create({
      title: 'Ubicación Guardada',
      subTitle: 'La ubicación fue actualizada con éxito',
      buttons: ['Ok']
      });
      alert.present();
    }, TIME_IN_MS);
  }
}
