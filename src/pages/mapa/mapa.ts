import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';
import { AlertController, LoadingController } from 'ionic-angular';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { ChatPage } from '../chat/chat';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';

import { FirebaseListObservable } from 'angularfire2/database';
import { FirebaseProvider } from './../../providers/firebase/firebase';


import { AngularFireDatabase } from 'angularfire2/database';

import * as firebase from 'firebase/app';

/**
 * Generated class for the MapaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {
	locationItems: FirebaseListObservable<any[]>;
	status=0;
	markers={};
	map: GoogleMap;
	rating=0;
 constructor(
	public afd: AngularFireDatabase,
	public firebaseProvider: FirebaseProvider,
	public navCtrl: NavController,
	public navParams: NavParams,
	private googleMaps: GoogleMaps,
	public menuCtrl: MenuController,
	private alertCtrl: AlertController,
	public loadingCtrl: LoadingController) {
 	this.locationItems = this.firebaseProvider.getLocationItems();
 	/*console.log("now?", this.locationItems);*/
 	
 	
 }
	// Load map only after view is initialized
	ngAfterViewInit() {
	 this.loadMap();
	}
	loadMap() {
		 // make sure to create following structure in your view.html file
		 // and add a height (for example 100%) to it, else the map won't be visible
		 // <ion-content>
		 //  <div #map id="map" style="height:100%;"></div>
		 // </ion-content>

		 // create a new map by passing HTMLElement
		 let element: HTMLElement = document.getElementById('map');

		 
		 this.map = this.googleMaps.create(element);
		 // listen to MAP_READY event
		 // You must wait for this event to fire before adding something to the map or modifying it in anyway
		 this.map.one(GoogleMapsEvent.MAP_READY).then(
		   () => {
		   	this.map.setClickable(false);
		     console.log('Map is ready!');
		     // Now you can add elements to the map like the marker
		   
		 
		 console.log('CameraPosition');
		 // create CameraPosition
		 let position: CameraPosition = {
		   target: {
		     lat: 19.4271695,
		     lng: -99.2103303
		   },
		   zoom: 16,
		   tilt: 30
		 };

		 // move the map's camera to position
		 this.map.moveCamera(position);
		 console.log('markeroptions');

		 // create new marker
		 let markerOptions: MarkerOptions = {
		   position: new LatLng(19.4277611 , -99.2099052 ),
		   title: 'Tu ubicación',
		  icon: {
		    url: 'img/car.png'
		   },
		     'styles' : {
    'text-align': 'center',
    'font-weight': 'bold',
    'color': '#222'
  }
		 };
		 console.log('addmarker');
		 this.map.addMarker(markerOptions)
		   .then((marker: Marker) => {
		      marker.showInfoWindow();
		    });
		});
	 }
//loadMap()

 	ionViewDidLoad(){
 	}

itemSelected(i){
	if(i==2){
		let loading = this.loadingCtrl.create({content: 'Buscando proveedores disponibles...'});
		loading.present();
		 // create new marker
		 let markerOptions: MarkerOptions = {
		   position: new LatLng(19.430987,-99.2117127),
		   title: 'Sebastián R.',
		   snippet: 'En camino',
  icon: {
    url: 'img/repair-icon.png'
   },
		   	  'styles' : {
    'text-align': 'center',
    'font-weight': 'bold',
    'color': '#222'
  }
		 };

		setTimeout( () => {
							 console.log('addmarker');
							 this.map.addMarker(markerOptions)
							   .then((marker: Marker) => {
							      marker.showInfoWindow();
							    });
							 // create CameraPosition
							 let position: CameraPosition = {
							   target: {
							     lat: 19.4269775,
							     lng: -99.2102333
							   },
							   zoom: 16,
							   tilt: 30
							 };

							 // move the map's camera to position
							 this.map.moveCamera(position);
							 console.log('markeroptions');
							loading.dismiss();
							this.status=i;
							}, 5000);

	} else{
		let loading = this.loadingCtrl.create({ content: 'Cargando...' });
		loading.present();
		setTimeout( () => {
				loading.dismiss();
				this.status=i;
			}, 1000);
	}
	
} //end ItemSelected
    chat(){
    this.navCtrl.setRoot(ChatPage);
  }
location(){
		let loading = this.loadingCtrl.create({content: 'Obteniendo tu ubicación...'});
		loading.present();
		setTimeout( () => {

				loading.dismiss();
				let alert = this.alertCtrl.create({
			      title: 'Ubicación Guardada',
			      subTitle: 'La ubicación fue actualizada con éxito',
			      buttons: ['Ok']
			      });
			      alert.present();
			}, 2000);
}
feedback(i){
	this.rating=i;

}
	card(){
		setTimeout( () => {
	      let alert = this.alertCtrl.create({
	      title: 'Cambiando Tarjeta',
	      buttons: ['Ok']
	      });
	      alert.present();
	    }, 500);
	}
}
