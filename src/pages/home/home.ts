import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LoginPage } from '../login/login';

import { MapaPage } from '../mapa/mapa';
import { MiembrosPage } from '../miembros/miembros';
import { ChatPage } from '../chat/chat';




@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	@ViewChild('email') email;
	@ViewChild('password') password;

  constructor( public navCtrl: NavController) {

  }



	goMap(){
    this.navCtrl.push(MapaPage)
	}
	goMembers(){
    this.navCtrl.push(MiembrosPage)
	}
	goChat(){
    this.navCtrl.push(ChatPage)
	}
}
