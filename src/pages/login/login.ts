import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { MapaPage } from '../mapa/mapa';
import { Storage } from '@ionic/storage';

import { Observable } from 'rxjs/Observable';

import { AngularFireAuth } from 'angularfire2/auth';

import * as firebase from 'firebase/app';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public loginuser: string = 'nzubiaur@enacment.com';
  public loginpwd: string = 'penguin2';
  user: Observable<firebase.User>;
  
  @ViewChild('email') email;
  @ViewChild('password') password;

  @ViewChild(Nav) nav: Nav;    
  constructor(private alertCtrl: AlertController , private fire:AngularFireAuth, public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad LoginPage')
  }


  alert(message:string){
    this.alertCtrl.create({
      title: 'Error al iniciar sesión',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }

  signIn(){
    //this.fire.auth.signInWithEmailAndPassword(this.email.value, this.password.value)
    this.fire.auth.signInWithEmailAndPassword('nzubiaur@enacment.com', 'penguin2')
    .then(data => {
      this.navCtrl.setRoot(MapaPage);
      console.log('data', data);
      console.log('fire auth', this.fire.auth);
      console.log('authstate', this.fire.authState);
    })
    .catch (error => {
      var errormsg = "";
      console.log('got an error', error);
      var ecode = error.message;
      if(ecode == "The email address is badly formatted."){
          errormsg = "Correo inválido. Verifique que sus datos sean correctos.";
      } else if(ecode == "The password is invalid or the user does not have a password."){
          errormsg = "Password incorrecto. Verifique su contraseña.";
      }else{
          errormsg =error.message;
      }
      this.alert(errormsg);
    })
    console.log(this.email.value, this.password.value);
  }

  

/*    getUser() {
      let userLogged;
      this.fire.authState
      this.fire.auth.onAuthStateChanged(function (user) {
        userLogged = user;
        console.log("E1l usuario   "+ userLogged);
       });
     }*/

}
