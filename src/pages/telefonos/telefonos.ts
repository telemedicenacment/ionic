import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';
/**
 * Generated class for the TelefonosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-telefonos',
  templateUrl: 'telefonos.html',
})
export class TelefonosPage {
	items=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public loadingCtrl: LoadingController) {
	this.items = [
	  {
	    "username" : "Waze",
	    "relacion" : "Activa",
	    "img"	:"img/waze.jpg",
	    "date"	:	"Febrero 2018"
	  },
	  {
	    "username" : "Coppel",
	    "relacion" : "Activa",
	    "img"	:"img/coppel.jpg",
	    "date"	:	"Septiembre 2017"
	  },
	  {
	    "username" : "ADO",
	    "relacion" : "Vencida",
	    "img"	:"img/ado.jpg",
	    "date"	:	"Marzo 2017"
	  }
	];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TelefonosPage');
  }
		add() {
		  let alert2 = this.alertCtrl.create({
		    title: 'Nueva Campaña',
		    inputs: [
		      {
		        name: 'username',
		        placeholder: 'ID Campaña',
		        type: 'text'
		      } ],
		    buttons: [
		      {
		        text: 'Cancelar',
		        role: 'cancel',
		        handler: data => {
		          console.log('Cancel clicked');
		        }
		      },
		      {
		        text: 'Guardar',
		        handler: data => {
		        	
					let loading = this.loadingCtrl.create({
						content: 'Buscando Campaña...'
					});
					loading.present();
				    let TIME_IN_MS = 3000;
				    let alert3 = setTimeout( () => {
				    					    	loading.dismiss();
				    }, TIME_IN_MS);
		        }
		      }
		    ]
		  });
		  alert2.present();
		}
delete(id){
    //You can add some logic here

    }
}
