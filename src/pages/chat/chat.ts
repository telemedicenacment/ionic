import { Component, ViewChild } from '@angular/core'; 
import { AlertController, LoadingController } from 'ionic-angular';
import { ModalController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { PerfilPage } from '../perfil/perfil';
import { Content } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
/**
 * Generated class for the MiembrosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
    @ViewChild(Content) content: Content;
    userId: string = '140000198202211138';
    userName: string = 'Nicolás Z.';
    userImgUrl: string = 'https://pbs.twimg.com/profile_images/651139349870841856/oQKVlnEI_400x400.jpg';
    toUserId: string = '210000198410281948';
    toUserName: string = 'Sebastián R.';
    editorMsg: string = '';
    msgList=[

        {
          "messageId":"4",
          "userId":"210000198410281948",
          "userName":"Sebastián R.",
          "userImgUrl":"img/avatar1.jpg",
          "toUserId":"140000198202211138",
          "toUserName":"Nicolás Z.",
          "toUserImgUrl":"https://pbs.twimg.com/profile_images/651139349870841856/oQKVlnEI_400x400.jpg",
          "time":Date.now(),
          "message":"Hola! Puedes indicarme tu ubicación?",
          "status":"success"
        },
        {
          "messageId":"6",
          "userId":"140000198202211138",
          "userName":"Nicolás Z.",
          "userImgUrl":"https://pbs.twimg.com/profile_images/651139349870841856/oQKVlnEI_400x400.jpg",
          "toUserId":"210000198410281948",
          "toUserName":"Sebastián R.",
          "toUserImgUrl":"img/avatar1.jpg",
          "time":Date.now(),
          "message":"Claro, estoy en la calle esperándote",
          "status":"success"
        },
        {
          "messageId":"5",
          "userId":"210000198410281948",
          "userName":"Sebastián R.",
          "userImgUrl":"img/avatar1.jpg",
          "toUserId":"140000198202211138",
          "toUserName":"Nicolás Z.",
          "toUserImgUrl":"https://pbs.twimg.com/profile_images/651139349870841856/oQKVlnEI_400x400.jpg",
          "time":Date.now(),
          "message":"Ya estoy en camino. Llego en 15 minutos.",
          "status":"success"
        }
      ];

  items=[];
    posts=[];
    user={};
    users=[];
    constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public afd: AngularFireDatabase)
   {

    const chats$ : FirebaseListObservable<any> = this.afd.list('chats-messages/circle4', {

      });
    chats$.subscribe(
      val => this.items=val 
      );
    const users$ : FirebaseListObservable<any> = this.afd.list('users', {
        query: {
          orderByChild: 'cid',
          equalTo: 'circle1' 
        }
      });
    users$.subscribe(
      val => this.users=val 
      );
    this.user ={
        "deviceToken" : "eDvqANggYwM:APA91bFKCT4RYvuAMI90gGFzbms5-ndQrtiPwe5BUo-jtVwMBcU3e5XcK1nIaWaLo7f6fPnBYDa8I-xqjW5Y7tUE9VgKbnarrVIiZj3ukNtNC_oI5fUhQB5zjyWEGnYSwUX1SKIuwT8G",
        "email" : "nzubiaur@enacment.com",
        "userImage" : "https://cache-graphicslib.viator.com/graphicslib/thumbs360x240/2635/SITours/pompeii-and-amalfi-coast-semi-private-day-trip-from-rome-in-rome-350763.jpg",
        "username" : "Nícolas",
        "uid": "7ykDx8EPcWMebnoO41DlVdDGKnG3"
     }
    }

  presentProfileModal() {
    let profileModal = this.modalCtrl.create(PerfilPage,{user:this.user});
    profileModal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MiembrosPage');

  }
  sendMsg(){
    if(this.editorMsg !== ""){
         let msg ={          
            messageId:'1',
          userId:'140000198202211138',
          userName:'Nicolás Z.',
          userImgUrl:'https://pbs.twimg.com/profile_images/651139349870841856/oQKVlnEI_400x400.jpg',
          toUserId:'210000198410281948',
          toUserName:'Sebastián R.',
          toUserImgUrl:'img/avatar1.jpg',
          time:Date.now(),
          message:this.editorMsg ,
          status:'succes'};

        this.msgList.push(msg);
        this.editorMsg = "";
        setTimeout( () => {this.content.scrollToBottom()}, 50);
                let ctr =Math.floor(Math.random() * 3) + 1;
                let msgtxt="Entendido";
                if(ctr==1){
                    msgtxt ="Perfecto";
                }else if(ctr==2){
                    msgtxt ="Claro";
                }
               let msg2 ={
                      messageId:"6",
                      userId:"210000198410281948",
                      userName:"Sebastián R.",
                      userImgUrl:"img/avatar1.jpg",
                      toUserId:"140000198202211138",
                      toUserName:"Nicolás Z.",
                      toUserImgUrl:"https://pbs.twimg.com/profile_images/651139349870841856/oQKVlnEI_400x400.jpg",
                      time:Date.now(),
                      message:msgtxt,
                      status:"success"
                    };
              setTimeout( () => {this.msgList.push(msg2);setTimeout( () => {this.content.scrollToBottom()}, 50)}, 1500);
    }
     

  } //SendMsg


}
