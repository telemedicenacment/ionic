import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';
/**
 * Generated class for the ServicePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-service',
  templateUrl: 'service.html',
})
export class ServicePage {
	services = [
			{name:"Grúa", icon:"car", id:0, class:""},
			{name:"Mecánico", icon:"build", id:1, class:""},
			{name:"Batería", icon:"battery-dead", id:2, class:""},
			{name:"Gasolina", icon:"color-fill", id:3, class:""}
		];
	date = moment().fromNow();
	status=0;
	statusarray=[
		{completed:false, body:'Orden Creada', current:true, date:moment().subtract(30, 'minutes').fromNow(), self:false},
		{completed:false, body:'En Proceso', current:false, date:moment().subtract(15, 'minutes').fromNow(), self:false},
		{completed:false, body:'Llegando a tu ubicación', current:false, date:moment().subtract(10, 'minutes').fromNow(), self:false},
		{completed:false, body:'Servicio en Progreso', current:false, date:moment().subtract(1, 'minutes').fromNow(), self:false}
	];
	hidden=true;
	map: GoogleMap;
	mylocation:any;
 constructor(
	public navCtrl: NavController,
	public navParams: NavParams,
	private googleMaps: GoogleMaps,
	private alertCtrl: AlertController,
	public loadingCtrl: LoadingController) {


  }
	ngAfterViewInit() {
	 this.loadMap();
	}
	hideswitch(){
		this.hidden=!this.hidden;
	}
	location(){
		this.map.getMyLocation().then((location)=>{
		let position: CameraPosition = {	
						target: {
						 lat: location.latLng.lat,
						 lng: location.latLng.lng
						},
						zoom: 16,
						tilt: 30
					};
		this.map.moveCamera(position);
		// create new marker
		let markerOptions: MarkerOptions = {
		position: new LatLng(location.latLng.lat, location.latLng.lng ),
		title: 'Tu ubicación',
		icon: {
			url: 'img/car.png'
			},
		'styles' : {
				    'text-align': 'center',
				    'font-weight': 'bold',
				    'color': '#222'
				  }
		};
		 this.map.addMarker(markerOptions)
		   .then((marker: Marker) => {
		      marker.showInfoWindow();
		    });
		});


	}
	selectService(selected){
		this.services[0].class=this.services[1].class=this.services[2].class = this.services[3].class="";
		this.services[selected.id].class="secondary";
	}

	loadMap() {
		 // make sure to create following structure in your view.html file
		 // and add a height (for example 100%) to it, else the map won't be visible
		 // <ion-content>
		 //  <div #map id="map" style="height:100%;"></div>
		 // </ion-content>

		 // create a new map by passing HTMLElement
		 let element: HTMLElement = document.getElementById('map');

		 
		 this.map = this.googleMaps.create(element);
		 // listen to MAP_READY event
		 // You must wait for this event to fire before adding something to the map or modifying it in anyway
		this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
			this.map.setClickable(false);
			this.location();
		});
	 }
//loadMap()
  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicePage');
   		let time=5000;
	    setTimeout( () => {
			this.statusarray[0].date= moment().subtract(30, 'minutes').fromNow();
			this.statusarray[0].completed=true;
			this.statusarray[0].current=true;
			this.statusarray[1].current=true;
			setTimeout( () => {
			this.statusarray[1].date= moment().subtract(17, 'minutes').fromNow();
			this.statusarray[1].completed=true;
			this.statusarray[1].current=true;
			this.statusarray[2].current=true;
				setTimeout( () => {
				this.statusarray[2].date= moment().subtract(10, 'minutes').fromNow();
				this.statusarray[2].completed=true;
				this.statusarray[2].current=true;
				this.statusarray[3].current=true;
					setTimeout( () => {
						this.statusarray[3].date= moment().subtract(5, 'minutes').fromNow();
						this.statusarray[3].completed=true;
						this.statusarray[3].current=false;
					},time);
				},time);
			},time);
		},time);

  }
itemSelected(i){
	if(i==2){
		let loading = this.loadingCtrl.create({content: 'Buscando proveedores disponibles...'});
		loading.present();
		 // create new marker
		setTimeout( () => {
							loading.dismiss();
							this.status=i;
							this.loadMap();
							}, 5000);

	} else{
		let loading = this.loadingCtrl.create({ content: 'Cargando...' });
		loading.present();
		setTimeout( () => {
				loading.dismiss();
				this.status=i;
				this.loadMap();
			}, 1000);
	}
	
} //end ItemSelected
}
